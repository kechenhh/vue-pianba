from flask import Flask, request
import requests
from lxml import etree
import urllib.parse
import re
import json
import time
from bs4 import BeautifulSoup

base_url = 'https://www.pianba.net/html/'
search_url = 'https://www.pianba.net/so/-------------/?wd='
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36'}

# 片吧获取影片方法
def mypiansou(pian):
    url = search_url + pian
    res = requests.get(url, headers=headers)
    html_doc = res.text
    soup = BeautifulSoup(html_doc, 'lxml')
    url_list = soup.find_all(name="a", attrs={"class": "stui-vodlist__thumb"})
    data_list = []
    for item in url_list:
        mydict = {}
        # 循环写入字典
        mydict['id'] = item.get('href').split("/")[-1].split(".")[0]
        mydict['title'] = item.get('title')
        mydict['pic_src'] = item.get('data-original')
        mydict['pic_text'] = item.find(name="span", attrs={"class": "pic-text"}).text
        # 插入数组
        data_list.append(mydict)
    return data_list

# 片吧获取下载链接方法
def getUrl(videoID):
    url = base_url + str(videoID) + '/'
    res = requests.get(url, headers=headers)
    html_doc = res.text
    soup = BeautifulSoup(html_doc, 'lxml')
    url_list = soup.find_all(name="span", attrs={"class": "down2"})
    data_list = []
    for item in url_list:
        mydict = {}
        # 循环写入字典
        mydict['video_url'] = item.find('a').get('href')
        mydict['video_title'] = mydict['video_url'].split("/")[-1].split(".")[0]
        # 插入数组
        data_list.append(mydict)
    return data_list


# 实例化
app = Flask(__name__)

# # 片吧获取影片接口 接收keywords 返回搜索结果
@app.route("/videolist", methods=["POST"])
def piansou():
    # 默认返回内容
    return_dict = {'return_code': '200', 'return_info': '处理成功', 'data': False}
    # 判断传入的json数据是否为空
    if request.get_data() is None:
        return_dict['return_code'] = '5004'
        return_dict['return_info'] = '请求参数为空'
        return json.dumps(return_dict, ensure_ascii=False)
    # 获取传入的参数
    get_Data = request.get_data()
    # 传入的参数为bytes类型，需要转化成json
    get_Data = json.loads(get_Data)

    mypian = get_Data.get('keywords')
    # # 对参数进行操作
    return_dict['data'] = mypiansou(mypian)
    return json.dumps(return_dict, ensure_ascii=False)

# # 片吧获取下载链接接口 接收videoId 返回下载链接
@app.route("/urllist", methods=["POST"])
def videolist():
    # 默认返回内容
    return_dict = {'return_code': '200', 'return_info': '处理成功', 'data': False}
    # 判断传入的json数据是否为空
    if request.get_data() is None:
        return_dict['return_code'] = '5004'
        return_dict['return_info'] = '请求参数为空'
        return json.dumps(return_dict, ensure_ascii=False)
    # 获取传入的参数
    get_Data = request.get_data()
    # 传入的参数为bytes类型，需要转化成json
    get_Data = json.loads(get_Data)
    videoID = get_Data.get('videoId')
    # 对参数进行操作
    return_dict['data'] = getUrl(videoID)
    return json.dumps(return_dict, ensure_ascii=False)

if __name__ == '__main__':
    # debug开启调试模式,host='0.0.0.0'允许任何ip访问
    app.run(host='0.0.0.0',port=5800)
