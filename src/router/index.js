//配置路由相关的信息
//导入
import VueRouter from 'vue-router'
import Vue from 'vue'
const Home = () => import('../components/Home')   //懒加载
const Detail = () => import('../components/Detail')

//解决跳转当前路由错误
const originalPush = VueRouter.prototype.push
   VueRouter.prototype.push = function push(location) {
   return originalPush.call(this, location).catch(err => err)
}

//1.通过Vue.use(插件),安装插件
Vue.use(VueRouter)

//2.创建VueRouter对象
const routes = [
  //在这里配置
  {
		path: '/',
		// redirect重定向
		redirect:'/home'
	},
	{
		path:'/home',
    component:Home,
    name:'首页'
	},
	{
		path:'/detail',
    component:Detail,
    name:'详情'  
	},

]
const router = new VueRouter({
	//配置路由和组件之间的应用关系
	routes
})

//3.将router 对象传入到Vue实例
//导出
export default router