// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'//导入
import VueClipboard from 'vue-clipboard2'

import { Button, Search, Col, Row, Checkbox, CheckboxGroup, Cell, CellGroup ,Toast,NavBar ,Loading, SwipeCell,Card ,Icon    } from 'vant'
import { Image as VanImage } from 'vant';
Vue.use(Button).use(Search).use(Col).use(Row).use(Checkbox).use(CheckboxGroup).use(Cell)
.use(CellGroup).use(Toast).use(NavBar).use(Loading).use(VanImage).use(SwipeCell).use(Card).use(Icon);
Vue.use(VueClipboard)


Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  router, //挂载
  template: '<App/>'
})
