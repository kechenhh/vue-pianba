// 首先引入我们自定义的axios对象
import request from '@/utils/request'

//搜索
export function videolist(data) {
  return request({
    url: '/api/videolist',
    method: 'post',
    data:data
  })
}

//影片详情
export function geturlList(data) {
  return request({
    url: '/api/urllist',
    method: 'post',
    data:data
  })
}